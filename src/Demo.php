<?php
namespace reusecom\comdemo;

class Demo
{
    public function hello()
    {
        return 'component demo';
    }

    public function noTest()
    {
        return 'no test';
    }

}