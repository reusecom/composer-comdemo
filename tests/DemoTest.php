<?php
namespace tests;

use PHPUnit\Framework\TestCase;
use reusecom\comdemo\Demo;

class DemoTest extends TestCase
{
    public function testHello()
    {
        $demo = new Demo();
        $res = $demo->hello();
        $this->assertEquals('component demo', $res);
    }
}